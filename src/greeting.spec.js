import React from 'react'
import { shallow, mount, render } from 'enzyme'

const Greeting = () => <div className='greeting'>Hello</div>

describe('A suite', function () {
  it('should render without throwing an error', function () {
    expect(shallow(<Greeting />).contains(<div className='greeting'>Hello</div>)).toBe(true)
  })

  it('should be selectable by class "greeting"', function () {
    expect(shallow(<Greeting />).is('.greeting')).toBe(true)
  })

  it('should mount in a full DOM', function () {
    expect(mount(<Greeting />).find('.greeting').length).toBe(1)
  })

  it('should render to static HTML', function () {
    expect(render(<Greeting />).text()).toEqual('Hello')
  })
})
